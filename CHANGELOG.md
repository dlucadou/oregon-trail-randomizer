# Changelog
All notable changes to this project will be documented in this file.

## [2.1.3] - 2022-06-06

### Added

- Icon source in readme

## [2.1.2] - 2021-12-25

### Changed

- Added screenshot to readme
- Fixed typos and grammar errors in readme
- Fixed formatting in license file

## [2.1.1] - 2021-04-01

### Changed

- Fixed a typo in the readme and improved formatting.
- Tim's pronouns in the readme. Congrats on finally coming out! https://twitter.com/MonotoneTim/status/1377388897794027526

## [2.1.0] - 2020-08-02

### Added

- Images for all other emotes
- Sounds for all other emotes

## [2.0.1] - 2020-03-21

### Changed

- Fixed incorrect cities being displayed for the year you depart

## [2.0.0] - 2020-03-21

### Added

- Changelog
- Enabled grandfather clocks
- `"use strict";` for JavaScript

### Changed

- Fixed mobile formatting

### Removed

- Most inline JavaScript

## [1.0.0] - 2018-10-20

(This is retroactively the 1.0.0 release, no changelog was made at this time.)
